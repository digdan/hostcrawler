<?php
	// Audio File Types
	$audio_files = array("wav","mp3","ogg","gsm","dct","flac","au","aiff","vox","raw","wma","aac","atrac","ra","ram","dss","msv","dvf","oma","omg","atp","mid","ape");

	// Video File Types
	$video_files = array("mp4","mpeg","avi","3g2","3gp","flv","m4v","mov","mpg","rm","srt","swf","vob","wmv","bin","bsf","cpi","dat","divx","edl","f4v","hdmov","m2p","mkv","mod","moi","mts","mxf","ogv","psh","r3d","rmvb","ts","webm","xvid","3gp2","3gpp");

	// Graphical File Types
	$pic_files = array("bmp","dds","gif","jpg","png","psd","pspimage","tga","thm","tif","tiff","yuv","3dm","3ds","max","obj","ai","eps","ps","svg");

	// Document File Types
	$doc_files = array("doc","docx","log","msg","odt","ibooks","pages","rtf","tex","txt","wpd","wps","xlr","xls","xlsx","ppt","pptx","indd","pct","pdf");

	// Data File Types
	$data_files = array("csv","dat","gbr","ged","key","keychain","pps","sdf","tar","zip","tax2012","vcf","xml","accdb","db","dbf","mdb","pdb","sql");

	// Executable File Types
	$exec_files = array("apk","app","bat","cgi","exe","dll");

	// Web File Types
	$web_files = array("html", "htm", "xml", "xhtml", "rss", "xht", "mht", "mhtml", "maff", "asp", "aspx", "bml", "cfm", "cgi", "ihtml", "jsp", "las", "lasso", "lassoapp", "pl", "php", "phtml", "rna", "r", "rnx", "shtml", "stm");

	$num_files = count($audio_files) + count($video_files) + count($pic_files) + count($doc_files) +count($data_files) + count($exec_files) + count($web_files);
?>
