<?PHP
if(!isset($_POST['url'])){
    header('Location: index.php');
}

ini_set('user_agent','A Friendly HostCrawler ( http://www.hostcrawler.com )');
/**
 * Spider - A PHP Website Crawler
 *
 * Its is not easy to download scrap a website for link to a certain type of
 * file online. Spider allows you to do just that. Simply specify what website
 * you want to search, the kind of files you are looking for then click Crawl
 * to watch spider handle the rest. 
 * With more than 150 file types pre-installed and multiple ways to view results, 
 * Spider is the tool at heart for people building crawlers and web scrappers.
 *
 * PHP version 5.4
 *
 * LICENSE: This source file is subject the CodeCanyon licenses
 * that are available through the world-wide-web at the following URI:
 * http://codecanyon.net/licenses.
 *
 * @category   PHP
 * @package    Spider
 * @author     Caesar Muakama <author@example.com>
 * @copyright  2013 Caesar Mukama
 * @license    http://codecanyon.net/licenses
 * @link       http://codecanyon.net/item/spider-php-website-crawler/5445643
 */

// {{{ Variables

/**
 * The url to be crawled
 *
 * This is the intial location to be search before
 * the crawler wonders deep into the website. The 
 * second line formats the url recieved.
 *
 * @var string
 */
$url = $_POST['url'];
$url = (0 !== strpos($url, 'http://') && 0 !== strpos($url, 'https://') && 0 !== strpos($url, 'ftp://')) ? "http://{$url}" : $_POST['url'];

/**
 * The depth of search
 *
 * Depth is the extent to which the crawler 
 * indexes pages within a website. Most sites 
 * contain multiple pages, which in turn can 
 * contain subpages. Default is 2.
 *
 * @var int
 */
$depth = $_POST['depth'] ? $_POST['depth'] : 2;

/**
 * The maximum number of results to be returned. 
 *
 * The crawler stop running when it finds the 
 * indicated number of results. Default is 50
 * 
 * @var int
 */
$limit = $_POST['limit'] ? $_POST['limit'] : 50;

/**
 * An array of the file types to be searched against. 
 * 
 * @var array
 */
$filetypes =  explode(',', $_POST['files']);

/**
 * The output format of the final results. 
 *
 * Potential values are 'html', 'csv', 'sql' and 'json'.
 * Default is 'html'.
 * 
 * @var string
 */
$output = $_POST['output'];

/**
 * The maximum number of seconds for the script to run. 
 *
 * Sometimes the crawler may have to take longer when 
 * searching many pages in order to get sparce results. 
 * Default: 60
 * 
 * @var int
 */
$seconds = $_POST['time'] ? $_POST['time'] : 60;

/// }}}

// {{{ Application Operations

/**
 * Application Specific Variables
 */
$time = date("m/d/Y h:i:s a", time() + $seconds);
$results = array();
$contents = "";

/*
 * Run the crawler.
 */
crawl_page($url, $depth);


/*
 * Process and print results found.
 */
$results = array_unique($results, SORT_REGULAR);

if(!empty($results)) { 
    if ($output === 'html') {
        $contents = "<html><body>";
        foreach ($results as $result) {
            $contents .= '<a href="'.$result['href'].'" target="_blank">'.$result['href']."</a><br>\n" ;
        }
        $contents .= "</body></html>";
    } elseif ($output === 'csv') {
        $contents = "URL\tExtension\tInner Text\n";
        foreach ($results as $result) {
            $contents .= $result['href']."\t".$result['innertext']."\t".$result['extension']."\n" ;
        }
    } elseif ($output === 'sql') {
        $contents = <<<EOT
CREATE TABLE IF NOT EXISTS `table_name`
( 
    `url` VARCHAR(255) NOT NULL, 
    `extension` VARCHAR(255) NOT NULL, 
    `inner_text` VARCHAR(255) NOT NULL
) 
ENGINE=MyISAM DEFAULT CHARSET=utf8; 

INSERT INTO `table_name` VALUES  
EOT;

    foreach ($results as $result) {
        $contents .= "('".$result['href']."', '".$result['innertext']."', '".$result['extension']."'),\n";
    }
    $contents = rtrim($contents, ",\n").";";

    } elseif ($output === 'json') {
        $contents = json_encode(array(
            'url' => $url,
            'depth' => $depth,
            'filetype' => $output,
            'results' => $results
            ));
    }

    $filename = 'results/' . generateRandomString() .'.'. $output;
    $file = fopen($filename,"w");
    fwrite($file, $contents);
    fclose($file);

    $data = array(
        'url' => $url,
        'depth' => $depth,
        'filetype' => $output,
        'filename' => $filename
        );

    echo json_encode($data);

} else {
    echo '0';
}

/// }}}

///{{{ Spider's Logic

/**
 * Crawl a web page
 *
 * A recursive function with which spider will download the first 
 * page and the attempt to read out all links in the that page. 
 * For each link found, run this function for more links.
 *
 * At each parse, spider will attemp to find links to the files 
 * that you are looking for. 
 *
 * @param string    $url     The url to be crawled
 * @return void
 *
 * @access public
 */
function crawl_page($url, $depth = 3) {
    global $limit;
    global $filetypes;
    global $output;
    global $time;

    global $results;

    static $seen = array();
    if (isset($seen[$url]) || $depth === 0 | $limit === 0 | date("m/d/Y h:i:s a") > $time) {
        return;
    }

    $seen[$url] = true;

    $dom = new DOMDocument('1.0');
    @$dom->loadHTMLFile($url);

    $anchors = $dom->getElementsByTagName('a');
    foreach ($anchors as $element) {
        $href = $element->getAttribute('href');
        $ext = get_file_extension($href);
        $innertext = $element->textContent;
        $active = $ext && in_array($ext, $filetypes);
	$urlParts = parse_url($href);
	if (!$urlParts['host']) {
            $path = '/' . ltrim($href, '/');
            if (extension_loaded('http')) {
                $href = http_build_url($url, array('path' => $path));
            } else {
                $parts = parse_url($url);
                $href = $parts['scheme'] . '://';
                if (isset($parts['user']) && isset($parts['pass'])) {
                    $href .= $parts['user'] . ':' . $parts['pass'] . '@';
                }
                $href .= $parts['host'];
                if (isset($parts['port'])) {
                    $href .= ':' . $parts['port'];
                }
                $href .= $path;
            }
        }

        if ($active){
		$dup = false;
	//Check results for existance of href already.
		foreach($results as $result) {
			if ($result['href'] == $href) {
				$dup = true;
			}
		}
		if (!$dup) {
        	    $result = array(
        	        'href' => $href, 
        	        'innertext' => $innertext,
        	        'extension' => $ext
        	        );

        	    array_push($results, $result);

        	    $limit--;
        	    if ($limit === 0) {
        	        break;
        	    }
		}
        }
        
        
        crawl_page($href, $depth - 1);
    }

}

/// }}}

/// {{{ Utitlity Functions

/**
 * Gets a file's extension
 *
 * Because sometimes a file's extention is not the last 3 charaters.
 *
 * @param string    $file_name     the assumed link from which to extract a filename
 * @return string   the extracted extension or false if no extension was discovered
 *
 * @access public
 */
function get_file_extension($file_name) {
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
    return strlen($ext) < 7 && strlen($ext) > 0 ? $ext : false;
}

/**
 * Generates random strings
 *
 * The strings are used as file names in which to store the
 * final results
 *
 * @param int   $length the length of the string
 * @return string   the random string
 *
 * @access public
 */
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

/// }}}


/*
 * tab-width: 4
 * c-basic-offset: 4
 * c-hanging-comment-ender-p: nil
 * End:
 */


?>
