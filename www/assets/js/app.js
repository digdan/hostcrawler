/*
 * Spider v1.1
 * http://codecanyon.net/item/spider-php-website-crawler/5445643
 */

$(function () {

	/* == Event Listeners ==*/

	// Enable help popovers
	$('.help').popover(
	{
		trigger: 'hover',
		html: true,
		placement: 'right',
		content: 'hello world'
	});

	// Selects multiple types with group buttons
	$('.types-group').change(function(){
		var group = $(this).val();
		var checked = this.checked;
		$('#' + group + ' .filetype').prop('checked', checked);
	});

	//Uncheck group checkbox when filetype is un checked
	$('.filetype').change(function(){
		this.checked ? '' : $('.types-group').prop('checked', false);
	})

	//Close popovers
	$('body').on('click', function (e) {
		$('.help').each(function () {
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				$(this).popover('hide');
			}
		});
	});

	//Try button action
	$(".try").on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: $(this.hash).offset().top }, 400);
	});

	/* == Data Connection == */
	//Crawl for results
	$('#search').submit(function(e){
		e.preventDefault();

		/* Validate everything */
		$('.error-container').hide();
		$('.has-error').removeClass('has-error');


		// Url is required
		var urlPattern = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
		if(!urlPattern.test($('#url').val())) {
			$('#url').closest('.form-group').addClass('has-error');
			$('.error-container').html('Valid url is required').slideDown();
			return;
		}

		// Atleast one filetype checkbox or other files text area
		if($('input.filetype:checkbox:checked').length == 0 && $('#otherFiles textarea').val() == "") {
			$('#otherFiles').closest('.form-group').addClass('has-error');
			$('.error-container').html('Select atleast one filetype OR enter Other File Types at the bottom.').slideDown();
			return;
		}

		// Depth Should be integer > 0
		if($('#depth').val()!=""){
			if(!isPositiveInteger($('#depth').val())){
				$('#depth').closest('.controls').addClass('has-error');
				$('.error-container').html('Depth should be a number larger than Zero.').slideDown();
				return;
			}
		}

		// Depth Should be less than 100
		if($('#depth').val() > 100){
			$('#depth').closest('.controls').addClass('has-error');
			$('.error-container').html('Depth should be less than 100.').slideDown();
			return;
		}

		// Crawl Time Should be integer > 0
		if($('#limit').val()!=""){
			if(!isPositiveInteger($('#limit').val())){
				$('#limit').closest('.controls').addClass('has-error');
				$('.error-container').html('Crawl Time should be a number larger than Zero.').slideDown();
				return;
			}
		}

		// Time Should be integer > 0
		if($('#time').val()!=""){
			if(!isPositiveInteger($('#time').val())){
				$('#time').closest('.controls').addClass('has-error');
				$('.error-container').html('Time should be a number larger than Zero.').slideDown();
				return;
			}
		}

		// Crawl Time Should be less than 1 hour
		if($('#time').val() > 3600){
			$('#time').closest('.controls').addClass('has-error');
			$('.error-container').html('Crawl Time should be less than 3600 seconds (1 hour).').slideDown();
			return;
		}

		//Extract selected file types
		var selectedFiles = "";
		$('input.filetype:checkbox:checked').each(function(){
			selectedFiles = selectedFiles + ',' + $(this).val()
		});
		if ($('#otherFiles textarea').val() != "") {
			selectedFiles = selectedFiles + ',' + $('#otherFiles textarea').val();
		}
		selectedFiles = selectedFiles.replace(/\s+|(^,)|(,$)/g, ""); //remove spaces, trailing and leading commas


		//Show that the app is fetching data 
		$('.results').css('background', "transparent");
		$('#crawl').attr("disabled", "disabled");
		var loader = '<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>';				/* Submit results */
		$('.results').html(loader);
		$('.download').hide();

		//Fetch results
		$.post("spider.php", { 

			url: $('#url').val(),
			files: selectedFiles, 
			depth: $('#depth').val(), 
			limit: $('#limit').val(),  
			time: $('#time').val(), 
			output: $('#output').val() 

		}).done(function(data) {
			//Process results
			if (data != '0') { //some data was found

				data = jQuery.parseJSON(data);
				if(data.filetype === 'html') {
					$('.results').css('background',"#222").load(data.filename);
				} else {
					$('.results').html('<pre></pre>');
					$('.results pre').load(data.filename)
				}
				$('.download').show().attr('href', data.filename);
				$('.download .downloadAs').html('.' + data.filetype)

			} else { //nothing was found

				$('.results').html('<p class="lead">Your search did not match any pages crawled.</p>\
					<p>Try increasing crawler depth or/and crawling time.</p>\
					<p>Ensure that the website link exists and the right filetype is selected.<p>');

			}

			//Enable search buttom
			$('#crawl').removeAttr('disabled');
		});


	})

	/* == Utility Functions == */
	//Tests positiveness of numbers
	function isPositiveInteger(n) {
		return n == 0 ? false : n >>> 0 === parseFloat(n);
	}
});
