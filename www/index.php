<?php
	include("types.php");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="An untility to help scrape and find specific media types on a given website.">
		<meta name="author" content="Danjelo Morgaux">
		<link rel="shortcut icon" href="assets/ico/favicon.png">

		<title>Host Crawler - A Website Media Scraper Utility</title>

		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
		<link href="assets/css/style.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="container">

			<!-- Header -->
			<div class="nav row">
				<div class='col-lg-4'>
					<a href="/"><img src="assets/images/logo.png" /></a>
				</div>
				<div class='col-lg-8'>
					<div class="marketing">
						<h3>An Online Website Media Scraper.</h3>
						<p>
							Host Crawler .com is a website spider and media crawler to help find and scrape videos, audio, graphics, documents, executables, data files, web pages and other file types. 
						</p>

					</div>
				</div>
			</div>
			<!-- End of Header -->

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- HostCrawler Leader -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7937169317942721"
     data-ad-slot="7269057383"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
	<BR><BR>


			<!-- Search Box -->
			<div id="app" class="col-md-5">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title">Search Box</h1>
					</div>
					<div class="panel-body">
						<form id="search" data-validate="parsley" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="url" class="col-lg-2 control-label">URL</label>
								<div class="col-lg-10">
									<input id="url" class="form-control" rows="3" placeholder="Try www.mp3skull.com"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Files Types</label>
								<div class="col-lg-10">

									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#audio">
														Audio
													</a>
													<input type="checkbox" value="audio" class="types-group">
												</h4>
											</div>
											<div id="audio" class="panel-collapse collapse out">
												<div class="panel-body types-list">
													<?php foreach ($audio_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#video">
														Video
													</a>
													<input type="checkbox" value="video" class="types-group">
												</h4>
											</div>
											<div id="video" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($video_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?> 
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#graphics">
														Graphics
													</a>
													<input type="checkbox" value="graphics" class="types-group">
												</h4>
											</div>
											<div id="graphics" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($pic_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#documents">
														Documents
													</a>
													<input type="checkbox" value="documents" class="types-group">
												</h4>
											</div>
											<div id="documents" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($doc_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#exec">
														Executables
													</a>
													<input type="checkbox" value="exec" class="types-group">
												</h4>
											</div>
											<div id="exec" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($exec_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#data">
														Data Files
													</a>
													<input type="checkbox" value="data" class="types-group">
												</h4>
											</div>
											<div id="data" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($data_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#web">
														Web Pages
													</a>
													<input type="checkbox" value="web" class="types-group">
												</h4>
											</div>
											<div id="web" class="panel-collapse collapse">
												<div class="panel-body types-list">
													<?php foreach ($web_files as $type) {
														echo '<label class="checkbox-inline"><input type="checkbox" class="filetype" value="'.$type.'"> '.$type.'</label>';
													}?>
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#otherFiles">
														Other File Types
													</a>
												</h4>
											</div>
											<div id="otherFiles" class="panel-collapse collapse">
												<div class="panel-body">
													<textarea class="form-control" rows="4" placeholder="use,commas,to,add,more,file,types"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Optional</label>
								<div class="options col-lg-10 controls">
									<div class="row">
										<div class="col-md-6">
											<label class="control-label">Depth: 
											</label>
											<span class="help glyphicon glyphicon-question-sign" data-toggle="popover" container="body" trigger="click" data-content="Depth is the extent to which the crawler indexes pages within a website. Most sites contain multiple pages, which in turn can contain subpages. Default: 2 (levels deep)"></span>
											<input id="depth" type="text" class="form-control" placeholder="2">
										</div>
										<div class="col-md-6">
											<label class="control-label">Max Results:
											</label>
											<span class="help glyphicon glyphicon-question-sign" data-toggle="tooltip" data-content="The maximum number of results to be returned. The crawler stop running when it finds the indicated number of results. Default: 50 (results)"></span>
											<input id="limit" type="text" class="form-control" placeholder="50">
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label class="control-label">Max Crawl Time:
											</label>
											<span class="help glyphicon glyphicon-question-sign" data-toggle="tooltip" data-content="The maximum allowed time (in seconds) within which the crawler may run. Sometimes the crawler may have to take longer when searching many pages in order to get sparce results. Default: 60 (seconds)"></span>
											<input id="time" type="text" class="form-control" placeholder="60">
										</div>

										<div class="col-md-6">
											<label class="control-label">Output:
											</label>
											<span class="help glyphicon glyphicon-question-sign" data-toggle="tooltip" data-content="Output indicates the filetype in which the results will be returned. Default: .HTML"></span>
											<select id="output" class="form-control">
												<option value="html">.HTML</option>
												<option value="csv">.CSV</option>
												<option value="sql">.SQL</option>
												<option value="json">.JSON</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="error-container alert alert-danger">
							</div>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10 text-right">
									<button id="crawl" type="submit" class="btn btn-primary btn-lg">Crawl Now <span class="glyphicon glyphicon-search"></span></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End of Search Box -->

			<!-- Results Box -->
			<div id="queryResults" class="col-md-7">
				<div class="results-box panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title">Results</h1>
					</div>
					<div class="panel-body">
						<div class="results">No results yet.</div>
						<BR><BR>
						<a class="download btn btn-success" target="_blank">Download (<span class="downloadAs"></span>) <span class="glyphicon glyphicon-download"></span></a>
					</div>
				</div>
			</div>
			<!-- End of Results Box -->

		</div> <!-- /container -->
		<div class='container text-center'>
			<div class="col-lg-12" style='color:white'>
				&copy; Copyright <?=date('Y')?>. Host Crawler .com
			</div>
			<BR><BR><BR>
		</div>



		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/app.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-19546749-19', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>
